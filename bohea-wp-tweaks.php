<?php
/*
Plugin Name: Bohea Wordpress Tweaks
Description: Common config tweaks.
Version: 1.0.0
Author: Matt Bohea
Author URI:  www.mattbohea.com
*/

// check for existing class
if(!class_exists("BoheaWpTweaks")) {

	// create class
	class BoheaWpTweaks {

    /*
     * Disable default dashboard widgets
     * Taken from Bones theme
     */
    function disable_default_dashboard_widgets() {
    	global $wp_meta_boxes;
    	// unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);    // Right Now Widget
    	// unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);     // Activity Widget
    	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // Comments Widget
    	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);  // Incoming Links Widget
    	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);         // Plugins Widget
    	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);       // Quick Press Widget
    	// unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);  // Recent Drafts Widget
    	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);           // Wordpress News
    	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);         //
    	// remove plugin dashboard boxes
    	unset($wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget']);           // Yoast's SEO Plugin Widget
    	unset($wp_meta_boxes['dashboard']['normal']['core']['rg_forms_dashboard']);        // Gravity Forms Plugin Widget
    	unset($wp_meta_boxes['dashboard']['normal']['core']['bbp-dashboard-right-now']);   // bbPress Plugin Widget
    }

    /*
     * Create client user role
     */
    function create_client_role() {
       global $wp_roles;
       if (!isset($wp_roles)){
           $wp_roles = new WP_Roles();
       }
       $editor = $wp_roles->get_role('editor');

       // add additional capabilities to the existing array
       $capabilities = $editor->capabilities;
       $capabilities['edit_theme_options'] = true;

       // create the 'client_administrator' role
       $wp_roles->add_role(
           'client_administrator',
           'Client Administrator',
           $capabilities
       );
    }

    /*
     * Remove specific menu items for non-admins
     * Edit as required
     */
    function remove_menus(){
      if ( !current_user_can( 'manage_options' ) ) {
        //remove_menu_page( 'index.php' );                  // Dashboard
        //remove_menu_page( 'edit.php' );                   // Posts
        //remove_menu_page( 'upload.php' );                 // Media
        //remove_menu_page( 'edit.php?post_type=page' );    // Pages
        //remove_menu_page( 'edit-comments.php' );          // Comments
        //remove_menu_page( 'themes.php' );                 // Appearance
        remove_menu_page( 'plugins.php' );                  // Plugins
        //remove_menu_page( 'users.php' );                  // Users
        remove_menu_page( 'tools.php' );                    // Tools
        remove_menu_page( 'options-general.php' );          // Settings

        // remove submenu items
		// @TODO These two don't work. Try to fix. Not essential as they don't appear on Client Admin role.
        remove_submenu_page( 'themes.php', 'theme-editor.php' );    // Theme Editor
        remove_submenu_page( 'plugins', 'plugin-editor.php' );      // Plugin Editor
      }
    }

    /*
     * Remove appearance > customize menu
     */
    function remove_customize_page(){
    	global $submenu;
    	unset($submenu['themes.php'][6]);
    }

    /*
     * Custom admin footer text
     * Taken from Bones theme
     */
    function custom_admin_footer() {
			// @TODO Test the UTM query string in GA
    	echo '<span id="footer-thankyou">Developed by <a href="http://mattbohea.com?utm=bohea-tweaks-plugin" target="_blank">Matt Bohea</a></span>.';
    }

	/*
	 * Remove admin bar for non-admins
	 */
	function remove_admin_bar() {
		if (!current_user_can('administrator') && !is_admin()) {
		  show_admin_bar(false);
		}
	}

    /*
     * Remove theme support
     * Optional, should ideally be done in the theme itself
     */
    function remove_theme_features(){
      remove_theme_support('custom-background');
      remove_theme_support('custom-header');
      remove_theme_support('post-formats');
    }

  	/*
  	 * Remove the p from around imgs
  	 * http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/
  	 * @TODO: check this is working
  	 */
  	function filter_ptags_on_images($content){
  		return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
  	}

  	/*
  	 * Remove the annoying […] to a Read More link
  	 * @TODO: This doesn't appear to be working. Investigate and fix.
  	 */
    /*
  	function excerpt_more($more) {
  		global $post;
  		// edit here if you like
  		return '...  <a class="excerpt-read-more" href="'. get_permalink( $post->ID ) . '" title="Read ' . esc_attr( get_the_title( $post->ID ) ).'">Read more &raquo;</a>';
  	} */

    /*
     * Move Yoast to bottom of post edit page
     */
    /*
    function bohea_yoasttobottom() {
    	return 'low';
    }
    add_filter('wpseo_metabox_prio', 'bohea_yoasttobottom');
    */

  } // close the class

} // close the class check


/*
 * If the class exists, assign a handler to it
 * @TODO Consult Scott on whether this approach is efficient.
 */
if(class_exists('BoheaWpTweaks')){
	$bohea_wp_tweaks = new BoheaWpTweaks();
    remove_action('welcome_panel', 'wp_welcome_panel' ); // Remove the Dashboard Welcome Panel
    add_action('wp_dashboard_setup', array($bohea_wp_tweaks, 'disable_default_dashboard_widgets'), 1);
    add_action('admin_init', array($bohea_wp_tweaks, 'create_client_role'), 1);
    add_action('admin_menu', array($bohea_wp_tweaks, 'remove_menus'), 999);
    add_action('admin_menu', array($bohea_wp_tweaks, 'remove_customize_page'), 999);
    add_filter('admin_footer_text', array($bohea_wp_tweaks, 'custom_admin_footer'), 1);
    add_action('after_setup_theme', array($bohea_wp_tweaks, 'remove_admin_bar'), 1);
    add_action('after_setup_theme', array($bohea_wp_tweaks, 'remove_theme_features'), 16);

	// if(!is_admin()) {
	//   add_action('init', array($bohea_wp_tweaks, 'filter_ptags_on_images'), 1);
	// 	add_action('init', array($bohea_wp_tweaks, 'excerpt_more'), 1);
	// }

} // close assignment of handler